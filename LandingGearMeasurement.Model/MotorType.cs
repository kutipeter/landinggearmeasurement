﻿namespace LandingGearMeasurement.Model
{
    public class MotorType
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual int IdCarModel { get; set; }

        public virtual CarModel CarModel { get; set; }
    }
}
