﻿using System.Collections.Generic;

namespace LandingGearMeasurement.Model
{
    public class CarModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual int IdManufacturer { get; set; }

        public virtual Manufacturer Manufacturer { get; set; }

        public virtual List<MotorType> MotorTypes { get; set; }
    }
}
