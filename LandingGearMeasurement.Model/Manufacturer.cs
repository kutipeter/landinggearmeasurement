﻿using System.Collections.Generic;

namespace LandingGearMeasurement.Model
{
    public class Manufacturer
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual List<CarModel> CarModels { get; set; }
    }
}
