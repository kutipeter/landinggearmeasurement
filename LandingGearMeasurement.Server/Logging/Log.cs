﻿using log4net;

namespace LandingGearMeasurement.Server.Logging
{
    public class Log
    {
        private static readonly Log Instance = new Log();
        protected ILog MonitoringLogger;
        protected static ILog DebugLogger;

        private Log()
        {
            MonitoringLogger = LogManager.GetLogger("MonitoringLogger");
            DebugLogger = LogManager.GetLogger("DebugLogger");
        }

        public static void Debug(string message)
        {
            DebugLogger.Debug(message);
        }

        public static void Debug(string message, System.Exception exception)
        {
            DebugLogger.Debug(message, exception);
        }


        public static void Info(string message)
        {
            Instance.MonitoringLogger.Info(message);
        }

        public static void Info(string message, System.Exception exception)
        {
            Instance.MonitoringLogger.Info(message, exception);
        }

        public static void Warn(string message)
        {
            Instance.MonitoringLogger.Warn(message);
        }

        public static void Warn(string message, System.Exception exception)
        {
            Instance.MonitoringLogger.Warn(message, exception);
        }

        public static void Error(string message)
        {
            Instance.MonitoringLogger.Error(message);
        }

        public static void Error(string message, System.Exception exception)
        {
            Instance.MonitoringLogger.Error(message, exception);
        }


        public static void Fatal(string message)
        {
            Instance.MonitoringLogger.Fatal(message);
        }

        public static void Fatal(string message, System.Exception exception)
        {
            Instance.MonitoringLogger.Fatal(message, exception);
        }


    }
}