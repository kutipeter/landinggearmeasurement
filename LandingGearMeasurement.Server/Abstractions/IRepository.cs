﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace LandingGearMeasurement.Server.Abstractions
{
    public interface IRepository<T> where T : class
    {
        T GetById(object id);
        
        void Insert(T entity);

        IQueryable<T> GetByFilter(Expression<Func<T, bool>> expression);
        
        T[] GetAll(bool hasTracking);

        void Delete(T entity);
        
        void CommitTransaction();
    }
}