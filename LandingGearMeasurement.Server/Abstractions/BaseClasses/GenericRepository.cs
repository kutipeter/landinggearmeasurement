﻿using LandingGearMeasurement.Server.DependencyResolution;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace LandingGearMeasurement.Server.Abstractions.BaseClasses
{
    public class GenericRepository<T> : IRepository<T> where T : class
    {
        public T GetById(object id)
        {
            return GetContext().Set<T>().Find(id);
        }

        public void Insert(T entity)
        {
            var context = GetContext();
            context.Set<T>().Add(entity);

            context.SaveChanges();
        }

        public IQueryable<T> GetByFilter(Expression<Func<T, bool>> filterExpression)
        {
            return GetContext().Set<T>().Where(filterExpression);
        }

        public T[] GetAll(bool hasTracking)
        {
            return GetContext().Set<T>().ToArray();
        }

        public void Delete(T entity)
        {
            var context = GetContext();

            context.Set<T>().Remove(entity);
            context.SaveChanges();
        }

        public void CommitTransaction()
        {
            GetContext().SaveChanges();
        }

        protected DbContext GetContext()
        {
            return IocController.GetInstance<DbContext>();
        }
    }

}