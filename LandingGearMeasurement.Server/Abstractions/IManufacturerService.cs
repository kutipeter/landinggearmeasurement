﻿using LandingGearMeasurement.Model;

namespace LandingGearMeasurement.Server.Abstractions
{
    public interface IManufacturerService
    {
        Manufacturer Get(int id);
        Manufacturer[] GetAll();
        void Add(string name);
        void Put(int id, string name);
        void Delete(int id);
    }
}