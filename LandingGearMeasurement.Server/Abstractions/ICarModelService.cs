﻿using LandingGearMeasurement.Model;

namespace LandingGearMeasurement.Server.Abstractions
{
    public interface ICarModelService
    {
        CarModel Get(int id);
        CarModel[] GetByManufacturer(int manufacturerId);
        CarModel[] GetAll();
        void Add(CarModel model);
        void Put(int id, string name);
        void Delete(int id);
    }
}