﻿using LandingGearMeasurement.Model;

namespace LandingGearMeasurement.Server.Abstractions
{
    public interface IMotorTypeService
    {
        MotorType Get(int id);
        MotorType[] GetByModel(int modelId);
        MotorType[] GetAll();
        void Add(MotorType motorType);
        void Put(int id, string name);
        void Delete(int id);
    }
}