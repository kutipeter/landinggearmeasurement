﻿using LandingGearMeasurement.Model;
using LandingGearMeasurement.Server.Abstractions;

namespace LandingGearMeasurement.Server.Services
{
    public class ManufacturerService : IManufacturerService
    {
        private readonly IRepository<Manufacturer> _manufacturerRepository;

        public ManufacturerService(IRepository<Manufacturer> manufacturerRepository)
        {
            _manufacturerRepository = manufacturerRepository;
        }

        public Manufacturer Get(int id)
        {
            return _manufacturerRepository.GetById(id);
        }

        public Manufacturer[] GetAll()
        {
            return _manufacturerRepository.GetAll(false);
        }

        public void Add(string name)
        {
            var manufacturer = new Manufacturer
            {
                Name = name
            };

            _manufacturerRepository.Insert(manufacturer);
        }

        public void Put(int id, string name)
        {
            var existingEntity = _manufacturerRepository.GetById(id);

            existingEntity.Name = name;

            _manufacturerRepository.CommitTransaction();
        }

        public void Delete(int id)
        {
            var entity = _manufacturerRepository.GetById(id);

            _manufacturerRepository.Delete(entity);

            _manufacturerRepository.CommitTransaction();
        }
    }
}