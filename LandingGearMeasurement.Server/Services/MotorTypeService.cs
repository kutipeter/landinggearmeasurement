﻿using System.Linq;
using LandingGearMeasurement.Model;
using LandingGearMeasurement.Server.Abstractions;

namespace LandingGearMeasurement.Server.Services
{
    public class MotorTypeService : IMotorTypeService
    {
        private readonly IRepository<MotorType> _motorTypeRepository;

        public MotorTypeService(IRepository<MotorType> motorTypeRepository)
        {
            _motorTypeRepository = motorTypeRepository;
        }

        public MotorType Get(int id)
        {
            return _motorTypeRepository.GetById(id);
        }

        public MotorType[] GetByModel(int modelId)
        {
            return _motorTypeRepository.GetByFilter(motorType => motorType.IdCarModel == modelId).ToArray();
        }

        public MotorType[] GetAll()
        {
            return _motorTypeRepository.GetAll(false);
        }

        public void Add(MotorType motorType)
        {
            _motorTypeRepository.Insert(motorType);
        }

        public void Put(int id, string name)
        {
            var existingEntity = _motorTypeRepository.GetById(id);

            existingEntity.Name = name;

            _motorTypeRepository.CommitTransaction();
        }

        public void Delete(int id)
        {
            var entity = _motorTypeRepository.GetById(id);

            _motorTypeRepository.Delete(entity);

            _motorTypeRepository.CommitTransaction();
        }
    }
}