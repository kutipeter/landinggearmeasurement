﻿using System.Linq;
using LandingGearMeasurement.Model;
using LandingGearMeasurement.Server.Abstractions;

namespace LandingGearMeasurement.Server.Services
{
    public class CarModelService : ICarModelService
    {
        private readonly IRepository<CarModel> _carModelRepository;

        public CarModelService(IRepository<CarModel> carModelRepository)
        {
            _carModelRepository = carModelRepository;
        }

        public CarModel Get(int id)
        {
            return _carModelRepository.GetById(id);
        }

        public CarModel[] GetByManufacturer(int manufacturerId)
        {
            return _carModelRepository.GetByFilter(carModel => carModel.IdManufacturer == manufacturerId).ToArray();
        }

        public CarModel[] GetAll()
        {
            return _carModelRepository.GetAll(false);
        }

        public void Add(CarModel model)
        {
            _carModelRepository.Insert(model);
        }

        public void Put(int id, string name)
        {
            var existingEntity = _carModelRepository.GetById(id);

            existingEntity.Name = name;

            _carModelRepository.CommitTransaction();
        }

        public void Delete(int id)
        {
            var entity = _carModelRepository.GetById(id);

            _carModelRepository.Delete(entity);

            _carModelRepository.CommitTransaction();
        }
    }
}