﻿InitializeManufacturers();

function InitializeManufacturers() {
    let model = new ManufacturersModel();

    model.initialize();

    ko.applyBindings(model);
}

function ManufacturersModel() {
    let _this = this;

    _this.manufacturersList = ko.observableArray([]);
    _this.currentModelsList = ko.observableArray([]);
    _this.currentMotorTypeList = ko.observableArray([]);


    _this.selectedManufacturer = ko.observable();
    _this.selectedModel = ko.observable();

    _this.isAddingManufacturer = ko.observable(false);
    _this.isAddingModel = ko.observable(false);
    _this.isAddingMotorType = ko.observable(false);

    _this.newManufacturerName = ko.observable("");
    _this.newModelName = ko.observable("");
    _this.newMotorTypeName = ko.observable("");

    _this.urlManifacturers = "api/manufacturers";
    _this.urlCarModels = "api/carmodels";
    _this.urlMotorTypes = "api/motortypes";

    _this.initialize = function() {
        _this.getManifacturers();
    };

    _this.changeAdditionState = function(addAvailable) {
        _this.isAddingManufacturer(addAvailable);
    };

    _this.changeModelAdditionState = function (addAvailable) {
        _this.isAddingModel(addAvailable);
    };

    _this.changeMotorTypeAdditionState = function (addAvailable) {
        _this.isAddingMotorType(addAvailable);
    };

    _this.onManufacturerSelected = function() {
        _this.getModels();
        _this.currentMotorTypeList([]);
    };

    _this.onModelSelected = function() {
        _this.getMotorTypes();
    };

    _this.saveNewManufacturer = function() {
        let url = `${window.baseUrl}${_this.urlManifacturers}/`;
        
        $.ajax({
            type: "POST",
            url: url,
            data: `=${_this.newManufacturerName()}` ,
            success: function(data) {
                _this.getManifacturers();
            }
        });
    };

    _this.saveNewModel = function() {
        let url = `${window.baseUrl}${_this.urlCarModels}/`;
        let model = {
            Name: _this.newModelName(),
            IdManufacturer: _this.selectedManufacturer().id
        };

        $.ajax({
            type: "POST",
            url: url,
            data: model,
            success: function(data) {
                _this.getModels();
            }
        });
    };

    _this.saveNewMotorType = function () {
        let url = `${window.baseUrl}${_this.urlMotorTypes}/`;
        let motorType = {
            Name: _this.newMotorTypeName(),
            IdCarModel: _this.selectedModel().id
        };

        $.ajax({
            type: "POST",
            url: url,
            data: motorType,
            success: function (data) {
                _this.getModels();
            }
        });
    };

    _this.getManifacturers = function () {
        let url = `${window.baseUrl}${_this.urlManifacturers}`;
        $.getJSON(url,
            function (response) {
                _this.manufacturersList(response);
            });
    };

    _this.getModels = function() {
        let url = `${window.baseUrl}${_this.urlCarModels}/bymanufacturer?manufacturerId=${_this.selectedManufacturer()
            .id}`;
        $.getJSON(url,
            function(response) {
                _this.currentModelsList(response);
            });
    };

    _this.getMotorTypes = function () {
        let url = `${window.baseUrl}${_this.urlMotorTypes}/bymodel?modelId=${_this.selectedModel().id}`;
        $.getJSON(url,
            function (response) {
                _this.currentMotorTypeList(response);
            });
    };
}