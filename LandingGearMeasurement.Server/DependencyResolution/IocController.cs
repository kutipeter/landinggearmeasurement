﻿using System.Data.Entity;
using System.Reflection;
using System.Web.Http;
using LandingGearMeasurement.Model;
using LandingGearMeasurement.Server.Abstractions;
using LandingGearMeasurement.Server.Abstractions.BaseClasses;
using LandingGearMeasurement.Server.Context;
using LandingGearMeasurement.Server.Services;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.WebApi;

namespace LandingGearMeasurement.Server.DependencyResolution
{
    public static class IocController
    {
        private static readonly Container Container;

        static IocController()
        {
            Container = new Container();
        }

        public static void Initialize()
        {
            Container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            
            RegisterAll();

            Container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
            Container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            Container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(Container);
        }

        public static T GetInstance<T>() where T : class
        {
            return Container.GetInstance<T>();
        }

        private static void RegisterAll()
        {
            Container.Register<DbContext>(() => new LandGearDbContext("LandGearDb"), Lifestyle.Scoped);

            Container.Register<IRepository<Manufacturer>, GenericRepository<Manufacturer>>(Lifestyle.Singleton);
            Container.Register<IRepository<CarModel>, GenericRepository<CarModel>>(Lifestyle.Singleton);
            Container.Register<IRepository<MotorType>, GenericRepository<MotorType>>(Lifestyle.Singleton);

            Container.Register<IManufacturerService, ManufacturerService>(Lifestyle.Singleton);
            Container.Register<ICarModelService, CarModelService>(Lifestyle.Singleton);
            Container.Register<IMotorTypeService, MotorTypeService>(Lifestyle.Singleton);
        }
    }
}