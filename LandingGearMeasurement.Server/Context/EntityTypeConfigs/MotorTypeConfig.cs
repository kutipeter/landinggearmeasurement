﻿using LandingGearMeasurement.Model;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LandingGearMeasurement.Server.Context.EntityTypeConfigs
{
    public class MotorTypeConfig : EntityTypeConfiguration<MotorType>
    {
        internal MotorTypeConfig()
        {
            SetPrimaryKey();

            SetMappings();

            SetKeyMappings();

            Map(config =>
            {
                config.ToTable("MotorTypes");
                config.MapInheritedProperties();
            });
        }

        private void SetPrimaryKey()
        {
            HasKey(_ => _.Id);

            Property(_ => _.Id)
                .HasColumnOrder(0)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }

        private void SetMappings()
        {
            var index = 0;

            Property(manufacturer => manufacturer.Name)
                .HasColumnName("Name")
                .HasColumnType("NVARCHAR")
                .HasColumnOrder(++index)
                .HasMaxLength(255)
                .IsRequired();
        }

        private void SetKeyMappings()
        {
            HasRequired(motorType => motorType.CarModel)
                .WithMany(carModel => carModel.MotorTypes)
                .HasForeignKey(motorType => motorType.IdCarModel);
        }
    }
}