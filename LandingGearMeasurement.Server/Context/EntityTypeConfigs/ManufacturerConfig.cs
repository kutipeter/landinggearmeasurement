﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using LandingGearMeasurement.Model;

namespace LandingGearMeasurement.Server.Context.EntityTypeConfigs
{
    internal class ManufacturerConfig : EntityTypeConfiguration<Manufacturer>
    {
        internal ManufacturerConfig()
        {
            SetPrimaryKey();

            SetMappings();
            
            Map(config =>
            {
                config.ToTable("Manufacturers");
                config.MapInheritedProperties();
            });
        }

        private void SetPrimaryKey()
        {
            HasKey(_ => _.Id);

            Property(_ => _.Id)
                .HasColumnOrder(0)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }

        private void SetMappings()
        {
            var index = 0;

            Property(manufacturer => manufacturer.Name)
                .HasColumnName("Name")
                .HasColumnType("NVARCHAR")
                .HasColumnOrder(++index)
                .HasMaxLength(255)
                .IsRequired();
        }
    }
}