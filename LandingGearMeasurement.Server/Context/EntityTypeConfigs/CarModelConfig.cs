﻿using LandingGearMeasurement.Model;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LandingGearMeasurement.Server.Context.EntityTypeConfigs
{
    public class CarModelConfig : EntityTypeConfiguration<CarModel>
    {
        internal CarModelConfig()
        {
            SetPrimaryKey();

            SetMappings();

            SetKeyMappings();

            Map(config =>
            {
                config.ToTable("CarModels");
                config.MapInheritedProperties();
            });
        }

        private void SetPrimaryKey()
        {
            HasKey(_ => _.Id);

            Property(_ => _.Id)
                .HasColumnOrder(0)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }

        private void SetMappings()
        {
            var index = 0;

            Property(manufacturer => manufacturer.Name)
                .HasColumnName("Name")
                .HasColumnType("NVARCHAR")
                .HasColumnOrder(++index)
                .HasMaxLength(255)
                .IsRequired();
        }

        private void SetKeyMappings()
        {
            HasRequired(carModel => carModel.Manufacturer)
                .WithMany(manufacturer => manufacturer.CarModels)
                .HasForeignKey(carModel => carModel.IdManufacturer);
        }
    }
}