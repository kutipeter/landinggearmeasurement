﻿using LandingGearMeasurement.Server.Context.EntityTypeConfigs;
using System.Data.Entity;

namespace LandingGearMeasurement.Server.Context
{
    public class LandGearDbContext : DbContext
    {
        public LandGearDbContext(string nameOrConnectionString) : base(nameOrConnectionString)
        { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ManufacturerConfig());
            modelBuilder.Configurations.Add(new CarModelConfig());
            modelBuilder.Configurations.Add(new MotorTypeConfig());

            base.OnModelCreating(modelBuilder);
        }
    }
}