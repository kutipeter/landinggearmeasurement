﻿using System.IO;
using System.Text;
using System.Web;
using LandingGearMeasurement.Server.Extensions;
using LandingGearMeasurement.Server.Logging;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace LandingGearMeasurement.Server.RequestCycle
{
    public class LogInterceptorAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var ipAddress = actionContext.Request.GetClientIpAddress();
            var body = GetRequestBody(actionContext);

            Log.Info($"Incoming request from {ipAddress}. Request details: API method: {actionContext.Request.RequestUri} " +
                     $"| Content: {body}");

            base.OnActionExecuting(actionContext);
        }

        private string GetRequestBody(HttpActionContext actionContext)
        {
            using (var stream = new StreamReader(actionContext.Request.Content.ReadAsStreamAsync().Result))
            {
                stream.BaseStream.Position = 0;
                return stream.ReadToEnd();
            }
        }
    }

}