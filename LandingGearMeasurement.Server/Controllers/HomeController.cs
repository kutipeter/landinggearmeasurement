﻿using System.Web.Mvc;

namespace LandingGearMeasurement.Server.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
