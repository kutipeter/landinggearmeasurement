﻿using LandingGearMeasurement.Server.Abstractions;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LandingGearMeasurement.Model;

namespace LandingGearMeasurement.Server.ApiControllers
{
    [RoutePrefix("api/motortypes")]
    public class MotorTypesApiController : ApiController
    {
        private readonly IMotorTypeService _motorTypeService;

        public MotorTypesApiController(IMotorTypeService motorTypeService)
        {
            _motorTypeService = motorTypeService;
        }

        [HttpGet, Route("")]
        public HttpResponseMessage Get()
        {
            try
            {
                var carModels = _motorTypeService.GetAll();

                return Request.CreateResponse(HttpStatusCode.OK, carModels);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Could not get all motor types. Please contact an administrator.");
            }
        }

        [HttpGet, Route("bymodel")]
        public HttpResponseMessage GetByModel(int modelId)
        {
            try
            {
                var carModels = _motorTypeService.GetByModel(modelId);

                return Request.CreateResponse(HttpStatusCode.OK, carModels);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Could not get all motor types. Please contact an administrator.");
            }
        }

        // GET api/<controller>/5
        [HttpGet, Route("{id}")]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var carModel = _motorTypeService.Get(id);

                return Request.CreateResponse(HttpStatusCode.OK, carModel);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    $"Could not get motor type for id {id}. Please contact an administrator.");
            }
        }

        [HttpPost, Route("")]
        public HttpResponseMessage Post([FromBody]MotorType model)
        {
            try
            {
                _motorTypeService.Add(model);

                return Request.CreateResponse(HttpStatusCode.Created);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    $"Could not insert motor type '{model.Name}'. Please contact an administrator.");
            }
        }

        [HttpPut, Route("{id}")]
        public HttpResponseMessage Put([FromUri]int id, [FromBody]string name)
        {
            try
            {
                _motorTypeService.Put(id, name);

                return Request.CreateResponse(HttpStatusCode.Accepted);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    $"Could not modify motor type '{name}'. Please contact an administrator.");
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete, Route("{id}")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                _motorTypeService.Delete(id);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    $"Could not delete motor type '{id}'. Please contact an administrator.");
            }
        }
    }
}