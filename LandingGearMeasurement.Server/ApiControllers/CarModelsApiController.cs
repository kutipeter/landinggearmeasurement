﻿using LandingGearMeasurement.Server.Abstractions;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LandingGearMeasurement.Model;

namespace LandingGearMeasurement.Server.ApiControllers
{
    [RoutePrefix("api/carmodels")]
    public class CarModelsApiController : ApiController
    {
        private readonly ICarModelService _carModelService;

        public CarModelsApiController(ICarModelService carModelService)
        {
            _carModelService = carModelService;
        }

        [HttpGet, Route("")]
        public HttpResponseMessage Get()
        {
            try
            {
                var carModels = _carModelService.GetAll();

                return Request.CreateResponse(HttpStatusCode.OK, carModels);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Could not get all car models. Please contact an administrator.");
            }
        }

        [HttpGet, Route("bymanufacturer")]
        public HttpResponseMessage GetByManufacturer(int manufacturerId)
        {
            try
            {
                var carModels = _carModelService.GetByManufacturer(manufacturerId);

                return Request.CreateResponse(HttpStatusCode.OK, carModels);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Could not get all car models. Please contact an administrator.");
            }
        }

        // GET api/<controller>/5
        [HttpGet, Route("{id}")]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var carModel = _carModelService.Get(id);

                return Request.CreateResponse(HttpStatusCode.OK, carModel);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    $"Could not get car model for id {id}. Please contact an administrator.");
            }
        }

        [HttpPost, Route("")]
        public HttpResponseMessage Post([FromBody]CarModel model)
        {
            try
            {
                _carModelService.Add(model);

                return Request.CreateResponse(HttpStatusCode.Created);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    $"Could not insert car model '{model.Name}'. Please contact an administrator.");
            }
        }

        [HttpPut, Route("{id}")]
        public HttpResponseMessage Put([FromUri]int id, [FromBody]string name)
        {
            try
            {
                _carModelService.Put(id, name);

                return Request.CreateResponse(HttpStatusCode.Accepted);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    $"Could not modify car model '{name}'. Please contact an administrator.");
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete, Route("{id}")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                _carModelService.Delete(id);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    $"Could not delete car model '{id}'. Please contact an administrator.");
            }
        }
    }
}