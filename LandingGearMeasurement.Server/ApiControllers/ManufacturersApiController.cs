﻿using LandingGearMeasurement.Server.Abstractions;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LandingGearMeasurement.Server.ApiControllers
{
    [RoutePrefix("api/manufacturers")]
    public class ManufacturersApiController : ApiController
    {
        private readonly IManufacturerService _manufacturerService;

        public ManufacturersApiController(IManufacturerService manufacturerService)
        {
            _manufacturerService = manufacturerService;
        }

        [HttpGet, Route("")]
        public HttpResponseMessage Get()
        {
            try
            {
                var manufacturers = _manufacturerService.GetAll();

                return Request.CreateResponse(HttpStatusCode.OK, manufacturers);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Could not get all manufacturers. Please contact an administrator.");
            }
        }

        // GET api/<controller>/5
        [HttpGet, Route("{id}")]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var manufacturer = _manufacturerService.Get(id);

                return Request.CreateResponse(HttpStatusCode.OK, manufacturer);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    $"Could not get manufacturer for id {id}. Please contact an administrator.");
            }
        }

        [HttpPost, Route("")]
        public HttpResponseMessage Post([FromBody]string name)
        {
            try
            {
                _manufacturerService.Add(name);

                return Request.CreateResponse(HttpStatusCode.Created);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    $"Could not insert manufacturer '{name}'. Please contact an administrator.");
            }
        }

        [HttpPut, Route("{id}")]
        public HttpResponseMessage Put(int id, [FromBody]string name)
        {
            try
            {
                _manufacturerService.Put(id, name);

                return Request.CreateResponse(HttpStatusCode.Accepted);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    $"Could not modify manufacturer '{name}'. Please contact an administrator.");
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete, Route("{id}")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                _manufacturerService.Delete(id);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                // TODO: LOG
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    $"Could not delete manufacturer '{id}'. Please contact an administrator.");
            }
        }
    }
}