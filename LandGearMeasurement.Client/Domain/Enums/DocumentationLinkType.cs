﻿namespace LandGearMeasurement.Client.Domain.Enums
{
    public enum DocumentationLinkType
    {
        Wiki,
        PageSource,
        ControlSource,
        StyleSource,
        Video
    }
}