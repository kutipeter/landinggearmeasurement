﻿using System;
using System.Diagnostics;

namespace LandGearMeasurement.Client.Infrastructure
{
    public class LimitedPoolItem<T> : IDisposable
    {
        private readonly Action<LimitedPoolItem<T>> _disposeAction;

        private TimeSpan _lifetime;
        private bool _expired;
        internal int DisposeFlag;

        public T Value { get; }

        internal bool Expired
        {
            get
            {
                if (_expired)
                    return true;
                _expired = _stopwatch.Elapsed > _lifetime;
                return _expired;
            }
        }

        private readonly Stopwatch _stopwatch;

        internal LimitedPoolItem(T value, Action<LimitedPoolItem<T>> disposeAction, TimeSpan lifetime)
        {
            _disposeAction = disposeAction;
            _lifetime = lifetime;
            Value = value;
            _stopwatch = Stopwatch.StartNew();
        }

        public void Expire()
        {
            _expired = true;
        }

        public void SetLifetime(TimeSpan newLifetime)
        {
            _lifetime = newLifetime;
            _stopwatch.Reset();
            _stopwatch.Start();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                _disposeAction(this);
            }
        }
    }
}
