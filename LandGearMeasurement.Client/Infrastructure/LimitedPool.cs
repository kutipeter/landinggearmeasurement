﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace LandGearMeasurement.Client.Infrastructure
{
    public class LimitedPool<T> : IDisposable where T : class
    {
        private readonly Func<T> _valueFactory;
        private readonly Action<T> _valueDisposeAction;
        private readonly TimeSpan _valueLifetime;
        private readonly ConcurrentStack<LimitedPoolItem<T>> _pool;
        private bool _disposed;

        public LimitedPool(Func<T> valueFactory, Action<T> valueDisposeAction, TimeSpan? valueLifetime = null)
        {
            _valueFactory = valueFactory;
            _valueDisposeAction = valueDisposeAction;
            _valueLifetime = valueLifetime ?? TimeSpan.FromHours(1);
            _pool = new ConcurrentStack<LimitedPoolItem<T>>();
        }

        public LimitedPoolItem<T> Get()
        {
            while (!_disposed && _pool.TryPop(out var item))
            {
                if (!item.Expired)
                {
                    return item;
                }
                item.Dispose();

                CollectAllExpiredItems();
            }

            return new LimitedPoolItem<T>(_valueFactory(), disposedItem =>
            {
                if (_disposed || disposedItem.Expired)
                {
                    if (Interlocked.CompareExchange(ref disposedItem.DisposeFlag, 1, 0) == 0)
                    {
                        _valueDisposeAction(disposedItem.Value);
                    }
                }
                else
                {
                    if (!_disposed)
                    {
                        _pool.Push(disposedItem);
                    }
                }
            }, _valueLifetime);
        }

        public LimitedPoolItem<T> Get(TimeSpan lifetime)
        {
            var item = Get();
            item.SetLifetime(lifetime);
            return item;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void ExpireAll()
        {
            foreach (var limitedPoolItem in _pool)
            {
                limitedPoolItem.Expire();
            }
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                _disposed = true;
                var items = _pool.ToArray();
                foreach (var item in items)
                {
                    _valueDisposeAction(item.Value);
                }
            }
        }

        private void CollectAllExpiredItems()
        {
            const int maximumBufferSize = 1000;
            var length = Math.Min(_pool.Count, maximumBufferSize);
            if (length <= 0)
            {
                return;
            }

            var items = new LimitedPoolItem<T>[length];
            var poppedItems = _pool.TryPopRange(items);
            for (var i = 0; i < poppedItems; i++)
            {
                var item = items[i];
                try
                {
                    item.Dispose();
                }
                catch
                {
                    var nextIndex = i + 1;
                    if (nextIndex < poppedItems)
                    {
                        _pool.PushRange(items, nextIndex, poppedItems - nextIndex);
                    }
                    throw;
                }
            }
        }
    }
}
