﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace LandGearMeasurement.Client.Infrastructure
{
    public class HttpClientPoolFactory
    {

        public LimitedPool<HttpClient> CreatePool()
        {
            return CreatePool(null);
        }

        public LimitedPool<HttpClient> CreatePool(int? timeoutSeconds)
        {
            return new LimitedPool<HttpClient>(() => CreateHttpClient(timeoutSeconds),
                client => client.Dispose());
        }

        private HttpClient CreateHttpClient(int? timeoutSeconds = null)
        {
            var client = new HttpClient(new HttpClientHandler
            {
                UseDefaultCredentials = true,
                AllowAutoRedirect = true,
                MaxAutomaticRedirections = 25
            }, false);
            client.DefaultRequestHeaders.Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/xml"));
            client.Timeout = timeoutSeconds == null
                ? TimeSpan.FromMinutes(5)
                : TimeSpan.FromSeconds((int)timeoutSeconds);

            return client;
        }
    }
}
