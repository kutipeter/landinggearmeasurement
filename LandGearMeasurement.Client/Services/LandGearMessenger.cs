﻿using System.Net.Http;
using LandGearMeasurement.Client.Infrastructure;
using LandingGearMeasurement.Model;
using Newtonsoft.Json;

namespace LandGearMeasurement.Client.Services
{
    public static class LandGearMessenger
    {
        private static readonly LimitedPool<HttpClient> _httpClientPool;

        static LandGearMessenger()
        {
            var poolFactory = new HttpClientPoolFactory();
            _httpClientPool = poolFactory.CreatePool();
        }

        public static Manufacturer[] GetAllManufacturers()
        {
            var requestUrl = "http://localhost:2471/api/manufacturers";

            using (var httpClient = _httpClientPool.Get())
            {
                var response = httpClient.Value.GetAsync(requestUrl).Result;
                response.EnsureSuccessStatusCode();

                var result = response.Content.ReadAsStringAsync().Result;

                return string.IsNullOrEmpty(result)
                    ? default
                    : JsonConvert.DeserializeObject<Manufacturer[]>(result);
            }
        }
    }
}
