﻿using LandingGearMeasurement.Model;
using System.ComponentModel;
using LandGearMeasurement.Client.Services;

namespace LandGearMeasurement.Client.ViewModels
{
    public class HomeViewModel : INotifyPropertyChanged
    {
        private Manufacturer[] _manufacturers;
        public Manufacturer[] Manufacturers
        {
            get => _manufacturers;
            set
            {
                _manufacturers = value;
                OnPropertyChanged("Manufacturers");
            }
        }

        private Manufacturer _selectedManufacturer;
        public Manufacturer SelectedManufacturer
        {
            get => _selectedManufacturer;
            set
            {
                _selectedManufacturer = value;
                OnPropertyChanged("SelectedManufacturer");
            }
        }

        private CarModel _selectedCarModel;
        public CarModel SelectedCarModel
        {
            get => _selectedCarModel;
            set
            {
                _selectedCarModel = value;
                OnPropertyChanged("SelectedManufacturer");
            }
        }

        private MotorType _selectedMotorType;
        public MotorType SelectedMotorType
        {
            get => _selectedMotorType;
            set
            {
                _selectedMotorType = value;
                OnPropertyChanged("SelectedMotorType");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public HomeViewModel()
        {
            GetAllManufacturers();
        }

        private void GetAllManufacturers()
        {
            Manufacturers = LandGearMessenger.GetAllManufacturers();
        }
    }
}
