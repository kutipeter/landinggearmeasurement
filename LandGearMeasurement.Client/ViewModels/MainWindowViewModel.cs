﻿using System;
using System.Windows.Controls;
using LandGearMeasurement.Client.Domain;
using MaterialDesignThemes.Wpf;

namespace LandGearMeasurement.Client.ViewModels
{
    public class MainWindowViewModel
    {
        public MainWindowViewModel(ISnackbarMessageQueue snackbarMessageQueue)
        {
            if (snackbarMessageQueue == null)
            {
                throw new ArgumentNullException(nameof(snackbarMessageQueue));
            }

            PageItems = new[]
            {
                new PageItem("Home", new Home {DataContext = new HomeViewModel()},
                    new []
                    {
                        DocumentationLink.PageLink<Home>(),
                        //DocumentationLink.PageLink<HtmlTestRecorder>()
                    })
                    {
                        VerticalScrollBarVisibilityRequirement = ScrollBarVisibility.Auto
                    },
                //new PageItem("Environment builder", new EnvironmentBuilder { DataContext = new EnvironmentBuilderViewModel() } ,
                //    new []
                //    {
                //        DocumentationLink.WikiLink("", "Wiki"),
                //        DocumentationLink.DashboardLink("TAC Dashboard"),
                //        DocumentationLink.PageLink<EnvironmentBuilder>(),
                //        DocumentationLink.PageLink<HtmlTestRecorder>()
                //    })
                //    {
                //        VerticalScrollBarVisibilityRequirement = ScrollBarVisibility.Auto
                //    }
            };
        }

        public PageItem[] PageItems { get; }
    }
}
